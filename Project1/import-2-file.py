### basic code to import a text file into a program

file = open("obama_inauguration_speech.txt", "r")

data = file.read()

#print(data)
print('The data is of type:', type(data))
print('The length of this string is:', len(data))

file.close()

