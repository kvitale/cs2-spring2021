#Week 5 scratch file
'''
#Warmup
file = open("../Project1-Orig/obama_inauguration_speech.txt", "r")
data = file.read()
xList = data.split()

for item in xList:
    print(item)



#Example of a pure function, i.e. a function that does not produce side effects
def doubleStuff(a_list):
    """ Return a new list in which contains doubles of the elements in a_list. """
    new_list = []
    for value in a_list:
        new_elem = 2 * value  #another comment may be helpful
        new_list.append(new_elem)
    return new_list

def changeTheName(someName):
    name = someName
    return name

things = [2, 5, 9]
print(things)
things = doubleStuff(things)
print(things)




#functions that produce lists
def evens_upto(n):
    """ Return a list of even numbers less than n. """
    result = []
    for i in range(2, n):
        if i%2 == 0:
            result.append(i)
    return result

evens = evens_upto(102)
print('Evens:', evens)

#for item in evens:
#    item.pop() #FILO





#Enumerate function
for idx, item in enumerate(things):
    #print(things)
    print(item,idx)
    if item % 10 == 0:
        print('it is!')
        things[idx] = 23

print(things)

'''





#Opens a file
fileref = open("week5-data.txt", "r")

#Read all data into a single string, also put the read "marker" at the end when finished
#myData = fileref.read()
#print(myData)

#A for loop will read each line at a time:
for aLine in fileref:
    #print(aLine)

    #add using the split method
    values = aLine.split()
    print(values[2], values[0], values[1], 'had a rating of ', values[10] )

#closes a file
fileref.close()





