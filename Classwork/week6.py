#week 6

#homework
'''
file = open('studentdata.txt', 'r')

for aLine in file:
    data = aLine.split()
    total = 0
    for i in data[1:]:
        total += float(i)
    avg = total / len(data[1:])
    print(data[0] + "'s", 'average is', avg)

file.close()

#Dictionary Alias vs Copy
opposites = {'up': 'down', 'right': 'wrong', 'true': 'false'}
alias = opposites

print(alias is opposites) #should be true

print(opposites['right'])
alias['right'] = 'left' #change a value in alias
print(opposites['right']) #that same value changed in opposites

#Copying
acopy = opposites.copy()

print(acopy is opposites) #should be false

acopy['right'] = 'LEFT'    # does not change opposites
print(opposites['right'])




#Dictionary Practice
### basic code to import a text file into a program

file = open("../Project1-Orig/obama_inauguration_speech.txt", "r")

data = file.read()

#print(data)
print(type(data))

print(len(data))
file.close()

words = { }
for item in data.split():
    if item not in words:
        words[item] = {}
        words[item]['count'] = 1
        words[item]['length'] = len(item)
    else:
        words[item]['count'] += 1
        words[item]['length'] = len(item)

print(words['the']['count'])
print(words['the']['length'])
'''
#print(words)

file = open("alice.txt", "r")

data = file.read()
print(data.split())
def cleanup(someString):
    specials = "‘“'-;:.,?&*()_!”"
    newString = ''
    for ltr in someString:
        if ltr not in specials:
            newString += ltr
    return newString

alice_words = {}

for item in data.split():
    if item not in alice_words:
        item = item.lower()
        item = cleanup(item)
        #print(item)
        alice_words[item] = {}
        alice_words[item]['count'] = 1
        alice_words[item]['length'] = len(item)
    else:
        alice_words[item]['count'] += 1
        alice_words[item]['length'] = len(item)

#print(alice_words)

#Finding the longest word
longest_word = ''
longest_word_length = 0

for k,v in alice_words.items():
    if alice_words[k]['length'] > longest_word_length:
        longest_word = k
        longest_word_length = alice_words[k]['length']
        print(longest_word, longest_word_length)

print(longest_word, longest_word_length)
