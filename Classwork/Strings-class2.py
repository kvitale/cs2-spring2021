#Examples from class 2

name = "Kyle"
day = "Friday"
greeting = "Hello {}, it is {}".format(name, day)
print(greeting)


#Traverse by Item
s = "python rocks"
for character_in_string in s:
    print(character_in_string)


for aname in ["Joe", "Amy", "Brad", "Angelina", "Zuki", "Thandi", "Paris"]:
    invitation = "Hi " + aname + ".  Please come to my party on Saturday!"
    print(invitation)


#Traverse by Index
fruit = "apple"
for idx in range(len(fruit)):
    print(fruit[idx])


#Accumulator Pattern
def removeVowels(s):
    vowels = "aeiouAEIOU"
    sWithoutVowels = ""
    for eachChar in s:
        if eachChar not in vowels:
            sWithoutVowels = sWithoutVowels + eachChar
    return sWithoutVowels

print(removeVowels("compsci"))
print(removeVowels("aAbEefIijOopUus"))
